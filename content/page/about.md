---
title: About me
subtitle: Thanks for reaching here~（づ￣3￣）づ╭❤～
comments: false
---

My name is Cheng Shen. I have the following hobbies:

- Dancing
- Cooking
- Watching TV shows/movies
- Reading

The probability I will do it decreases from top to down.

### my history

I have been to most of European Schengen countries and Israel. This is not completely due to my enthusiasm for traveling but also the result of study-abroad programs supported by my country after its economic takeoff. I think my upbringing synchronizes with globalization and benefited from it. As a global citizen, I am proud of my cultural identity as well as love to embrace diversity.
